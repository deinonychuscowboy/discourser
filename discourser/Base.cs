﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Discourser {
	/// <summary>
	/// Layer of language construction determines how individual constructs relate to each other
	/// </summary>
	public enum Layer{
		PHONETICS,
		PHONOLOGY,
		MORPHOLOGY_CLUSTER,
		MORPHOLOGY_SYLLABLE,
		MORPHOLOGY_MORPHEME,
		SYNTAX,
		CONTEXT, // Semantics, Pragmatics, Stylistics, and Semiotics are too complex to really handle in a program like this, but we can provide some rudimentary understanding of things like formality registers via a context layer.
	}

	public class Affiliate{
		public Phonetics.Phonetics Phonetics{ get; private set;}
		public Phonology.Phonology Phonology{ get; private set; }
		public Morphology.Morphology Morphology{ get; private set; }
		//public Syntax.Syntax Syntax{ get; private set; }
		//public Context.Context Context{ get; private set; }

		public Dictionary<Layer,Construct[]> Fragments{
			get{
				return new Dictionary<Layer,Construct[]>{
					{Layer.PHONETICS,this.Phonetics.Phoneticon},
					{Layer.PHONOLOGY,this.Phonology.Phonemicon},
					{Layer.MORPHOLOGY_CLUSTER,this.Morphology.Clustericon},
					{Layer.MORPHOLOGY_SYLLABLE,this.Morphology.Syllabicon},
					{Layer.MORPHOLOGY_MORPHEME,this.Morphology.Morphemicon},
					//{Layer.SYNTAX,this.Syntax.Fragments},
					//{Layer.CONTEXT,this.Context.Fragments},
				};
			}
		}

		public void Register(Phonetics.Phonetics p){
			this.Phonetics=p;
		}

		public void Register(Phonology.Phonology l){
			this.Phonology=l;
		}

		public void Register(Morphology.Morphology m){
			this.Morphology=m;
		}
		/*
		public void Register(Syntax.Syntax s){
			this.Syntax=s;
		}
		*/
		/*
		public void Register(Context.Context c){
			this.Context=c;
		}
		*/
	}

	/// <summary>
	/// Corresponding to a layer, a facet implements the langauge's concept of the layer
	/// </summary>
	public interface Facet{
		IEnumerable<Construct> Select(Condition c);
		Affiliate Affiliate{ get; }
	}

	/// <summary>
	/// Interface for meta-information that can be attached to a construct
	/// </summary>
	public abstract class Meta{
		string Representation{get;}
		public override bool Equals(object obj){
			if(this is EnumeratedMeta) {
				return ((EnumeratedMeta)this).Equals(obj);
			} else return base.Equals(obj);
		}
	}

	/// <summary>
	/// Meta-information based on enumerated values or categories
	/// </summary>
	public class EnumeratedMeta:Meta{
		public Enum Category{ get; }
		public Type CategoryType{ get; }
		public EnumeratedMeta(Enum category){
			this.Category=category;
			this.CategoryType=category.GetType();
		}
		public override bool Equals(object obj) {
			bool val1=obj.GetType()==typeof(EnumeratedMeta);
			bool val2=((EnumeratedMeta)obj).CategoryType==this.CategoryType;
			bool val3=Convert.ToInt32(((EnumeratedMeta)obj).Category)==Convert.ToInt32(this.Category);
			bool val4=obj is Enum && Convert.ToInt32(obj)==Convert.ToInt32(this.Category);
			bool val5=obj is Type&&((Type)obj)==this.CategoryType;

			return val1&&val2&&val3||val4||val5;
		}

		public static implicit operator EnumeratedMeta(Enum e){
			return new EnumeratedMeta(e);
		}

		public string Representation { get { return Enum.GetName(this.CategoryType,this.Category); } }
	}

	public class MetaComparer:IEqualityComparer<Meta>{
		public bool Equals(Meta a,Meta b){
			return a.Equals(b);
		}

		public int GetHashCode(Meta obj) {
			return obj.GetHashCode();
		}
	}

	/// <summary>
	/// A linguistic construct is the basic, generalized unit of all elements of the language.
	/// </summary>
	public class Construct{
		/// <summary>
		/// Constructs may be composed of more fine-grained constructs or of variant alternative constructs
		/// </summary>
		public Construct[] Composition{get;}
		/// <summary>
		/// What layer this construct is relevant to
		/// </summary>
		public Layer Layer{ get; }
		/// <summary>
		/// String representation of the construct
		/// </summary>
		public string Representation{get;}
		/// <summary>
		/// Meta-information about this construct
		/// </summary>
		public Meta[] Meta{ get; }

		public Construct(Layer layer,string representation,Meta[] meta,Construct[] composition){
			this.Composition=composition;
			this.Layer=layer;
			this.Representation=representation;
			this.Meta=meta??new Meta[]{};
		}
		public override bool Equals(object obj) {
			if(obj is Construct) {
				Construct other=(Construct)obj;
				if(this.Layer!=other.Layer) {
					return false;
				}
				if(this.Composition.Length==other.Composition.Length) {
					for(int i=0;i<this.Composition.Length;i++) {
						if(this.Composition[i]!=other.Composition[i]) {
							return false;
						}
					}
				}
				if(this.Meta.Length==other.Meta.Length) {
					for(int i=0;i<this.Meta.Length;i++) {
						if(this.Meta[i]!=other.Meta[i]) {
							return false;
						}
					}
				}
				return true;
			} else if(obj is Construct[]) {
				Construct[] other=(Construct[])obj;
				bool match=true;
				if(this.Composition.Length==other.Length) {
					for(int i=0;i<this.Composition.Length;i++) {
						if(this.Composition[i]!=other[i]) {
							match=false;
							break;
						}
					}
				}
				return match;
			}
			return base.Equals(obj);
		}
	}

	/// <summary>
	/// At language parsing time, a condition is used to provide contextual information relative to this construct which helps in narrowing down any possible alternatives
	/// </summary>
	public struct Condition{
		public Meta[] Selector{get;}
		private bool any;
		private Condition(Meta[] selector, bool any){
			this.Selector=selector;
			this.any=any;
		}
		public static Condition All(Meta[] selector){
			return new Condition(selector,false);
		}
		public static Condition Any(Meta[] selector){
			return new Condition(selector,true);
		}
		public bool Test(Construct test){
			if(this.any){
				foreach(Meta m in this.Selector) {
					if(test.Meta.Contains(m)) {
						return true;
					}
				}
				return false;
			}else{
				foreach(Meta m in this.Selector) {
					if(!test.Meta.Contains(m)) {
						return false;
					}
				}
				return true;
			}
		}
	}

	public struct Rule{
		public Layer Layer{get;}
		public Condition Condition{ get; }
		public RuleSet Children{ get; }
		public Meta[] Meta{ get; private set;}
		private Applicability applicability;
		public Rule(Layer layer,Condition condition):this(layer,condition,new RuleSet(),new Meta[]{},null){}
		public Rule(Layer layer,Condition condition, RuleSet children):this(layer,condition,children,new Meta[]{},null){}
		public Rule(Layer layer,Condition condition, Applicability applicability):this(layer,condition,new RuleSet(),new Meta[]{},applicability){}
		public Rule(Layer layer,Condition condition, Meta[] meta):this(layer,condition,new RuleSet(),meta,null){}
		public Rule(Layer layer,Condition condition, RuleSet children,Meta[] meta):this(layer,condition,children,meta,null){}
		public Rule(Layer layer,Condition condition, Meta[] meta, Applicability applicability):this(layer,condition,new RuleSet(),meta,applicability){}
		public Rule(Layer layer,Condition condition, RuleSet children, Meta[] meta, Applicability applicability){
			this.Layer=layer;
			this.Condition=condition;
			this.Children=children;
			this.Meta=meta;
			this.applicability=applicability;
		}
		public bool Applicable(Construct subj){
			if(this.applicability==null) {
				return this.Condition.Test(subj);
			} else {
				return this.applicability(subj,this.Condition);
			}
		}
		public void AddMeta(Meta m){
			this.Meta=this.Meta.Concat(new Meta[]{ m }).ToArray();
		}
	}

	public struct RuleSet{
		public Rule[] Rules{ get; }
		public Meta[] Meta { 
			get { 
				return this.Rules.Aggregate(
					(Meta[])null,
					(carry,x) => {
						return carry==null?x.Meta:carry.Where(y => x.Meta.Contains(y)).ToArray();
					}
				); 
			} 
		}
		public RuleSet(params Rule[] rules){
			this.Rules=rules;
		}

		public void AddMeta(Meta m){
			foreach(Rule r in this.Rules){
				if(!r.Meta.Contains(m)) {
					r.AddMeta(m);
				}
			}
		}
	}

	/// <summary>
	/// An applicability function is used to determine a true/false value for a given set of conditions.
	/// For instance, a function might return true if Layer.CONTEXT contains a formal register, and false otherwise.
	/// </summary>
	public delegate bool Applicability(Construct subj,Condition c);
	public delegate Construct[] Selector(Rule r);

	public class Temp{
		public static void Main(string[] args){
			Affiliate a=new Affiliate();
			// constructing a language begins by defining the phones (actual sounds) used in the language
			Phonetics.Phonetics p=new Phonetics.SpatialPhonetics(a,"b","p","t","d","k","g","bʰ","pʰ","tʰ","dʰ","kʰ","gʰ","f","v","s","z","ʃ","ʒ","h","ɹ","l","j","m","n","i","a","a̴","u","o","e","e˞","θ","ð","t͡ʃ","d͡ʒ");
			// phones are grouped into phonemes (sounds recognized by native speakers as "distinct sounds")
			Phonology.Phonology l=new Phonology.IPABasedPhonology(a,new Discourser.Phonology.IPAPhonologyInstruction[]{
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantPlace.PREALVEOLAR,Phonetics.IPAPhoneticConsonantObject.LABIAL,Phonetics.IPAPhoneticConsonantManner.STOP}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantPlace.ALVEOLAR,Phonetics.IPAPhoneticConsonantManner.STOP}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantPlace.VELAR,Phonetics.IPAPhoneticConsonantManner.STOP}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantPlace.PREALVEOLAR,Phonetics.IPAPhoneticConsonantObject.LABIAL|Phonetics.IPAPhoneticConsonantObject.DENTAL,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantPlace.ALVEOLAR,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantPlace.POSTALVEOLAR,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantPlace.PREALVEOLAR,Phonetics.IPAPhoneticConsonantObject.DENTAL|Phonetics.IPAPhoneticConsonantObject.LINGUAL,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICELESS,Phonetics.IPAPhoneticConsonantManner.STOP,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}), // affricates

				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantPlace.PREALVEOLAR,Phonetics.IPAPhoneticConsonantObject.LABIAL,Phonetics.IPAPhoneticConsonantManner.STOP}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantPlace.ALVEOLAR,Phonetics.IPAPhoneticConsonantManner.STOP}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantPlace.VELAR,Phonetics.IPAPhoneticConsonantManner.STOP}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantPlace.PREALVEOLAR,Phonetics.IPAPhoneticConsonantObject.LABIAL|Phonetics.IPAPhoneticConsonantObject.DENTAL,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantPlace.ALVEOLAR,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantPlace.POSTALVEOLAR,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantPlace.PREALVEOLAR,Phonetics.IPAPhoneticConsonantObject.DENTAL|Phonetics.IPAPhoneticConsonantObject.LINGUAL,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantVoicing.VOICED,Phonetics.IPAPhoneticConsonantManner.STOP,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}), // affricates

				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantPlace.GLOTTAL,Phonetics.IPAPhoneticConsonantManner.FRICATIVE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantManner.APPROXIMANT}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantManner.NASAL}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.VOWEL,Phonetics.IPAPhoneticVowelDepth.FRONT,Phonetics.IPAPhoneticVowelHeight.CLOSE}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.VOWEL,Phonetics.IPAPhoneticVowelDepth.FRONT,Phonetics.IPAPhoneticVowelHeight.OPEN}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.VOWEL,Phonetics.IPAPhoneticVowelDepth.BACK}),
				Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.VOWEL,Phonetics.IPAPhoneticVowelDepth.FRONT,Phonetics.IPAPhoneticVowelHeight.CLOSE_MID}),
			});

			Morphology.Morphology m=new Morphology.SyllabicMorphology(a,new RuleSet[]{
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_CLUSTER,
						Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantManner.STOP}),
						new RuleSet(
							new Rule(
								Layer.PHONOLOGY,
								Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticConsonantVariant.ASPIRATED})
							)
						),
						new Meta[]{new EnumeratedMeta(Morphology.ClusterPostion.FINAL)}
					)
				),
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_CLUSTER,
						Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantManner.STOP}),
						new RuleSet(
							new Rule(
								Layer.PHONOLOGY,
								Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticConsonantVariant.PLAIN})
							)
						),
						new Meta[]{new EnumeratedMeta(Morphology.ClusterPostion.INITIAL)}
					),
					new Rule(
						Layer.MORPHOLOGY_CLUSTER,
						Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantManner.APPROXIMANT}),
						new Meta[]{new EnumeratedMeta(Morphology.ClusterPostion.INITIAL)}
					)
				),
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_CLUSTER,
						Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantManner.APPROXIMANT}),
						new Meta[]{new EnumeratedMeta(Morphology.ClusterPostion.FINAL)}
					),
					new Rule(
						Layer.MORPHOLOGY_CLUSTER,
						Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.CONSONANT,Phonetics.IPAPhoneticConsonantManner.STOP}),
						new RuleSet(
							new Rule(
								Layer.PHONOLOGY,
								Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticConsonantVariant.ASPIRATED})
							)
						),
						new Meta[]{new EnumeratedMeta(Morphology.ClusterPostion.FINAL)}
					)
				),
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_CLUSTER,
						Condition.All(new EnumeratedMeta[]{Phonetics.IPAPhoneticType.VOWEL}),
						new Meta[]{new EnumeratedMeta(Morphology.ClusterPostion.MEDIAL)}
					)
				),
			},new RuleSet[]{
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_SYLLABLE,
						Condition.All(new EnumeratedMeta[]{Morphology.ClusterPostion.INITIAL}),
						new Meta[]{new EnumeratedMeta(Morphology.SyllablePosition.INITIAL),new EnumeratedMeta(Morphology.SyllablePosition.MEDIAL)}
					),
					new Rule(
						Layer.MORPHOLOGY_SYLLABLE,
						Condition.All(new EnumeratedMeta[]{Morphology.ClusterPostion.MEDIAL}),
						new Meta[]{new EnumeratedMeta(Morphology.SyllablePosition.INITIAL),new EnumeratedMeta(Morphology.SyllablePosition.MEDIAL)}
					)
				),
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_SYLLABLE,
						Condition.All(new EnumeratedMeta[]{Morphology.ClusterPostion.INITIAL}),
						new Meta[]{new EnumeratedMeta(Morphology.SyllablePosition.FINAL)}
					),
					new Rule(
						Layer.MORPHOLOGY_SYLLABLE,
						Condition.All(new EnumeratedMeta[]{Morphology.ClusterPostion.MEDIAL}),
						new Meta[]{new EnumeratedMeta(Morphology.SyllablePosition.FINAL)}
					),
					new Rule(
						Layer.MORPHOLOGY_SYLLABLE,
						Condition.All(new EnumeratedMeta[]{Morphology.ClusterPostion.FINAL}),
						new Meta[]{new EnumeratedMeta(Morphology.SyllablePosition.FINAL)}
					)
				),
			},new RuleSet[]{
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_MORPHEME,
						Condition.All(new EnumeratedMeta[]{Morphology.SyllablePosition.FINAL})
					)
				),
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_MORPHEME,
						Condition.All(new EnumeratedMeta[]{Morphology.SyllablePosition.INITIAL})
					),
					new Rule(
						Layer.MORPHOLOGY_MORPHEME,
						Condition.All(new EnumeratedMeta[]{Morphology.SyllablePosition.FINAL})
					)
				),
				new RuleSet(
					new Rule(
						Layer.MORPHOLOGY_MORPHEME,
						Condition.All(new EnumeratedMeta[]{Morphology.SyllablePosition.INITIAL})
					),
					new Rule(
						Layer.MORPHOLOGY_MORPHEME,
						Condition.All(new EnumeratedMeta[]{Morphology.SyllablePosition.MEDIAL})
					),
					new Rule(
						Layer.MORPHOLOGY_MORPHEME,
						Condition.All(new EnumeratedMeta[]{Morphology.SyllablePosition.FINAL})
					)
				),
			});
			// TODO finish implementing heirarchical rules
			// TODO expanding rulesets?
		}
	}
}

