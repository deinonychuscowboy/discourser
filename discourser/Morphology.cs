﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Discourser.Morphology {
	
	public interface Morphology:Facet{
		Construct[] Morphemicon{ get; }
		Construct[] Syllabicon{ get; }
		Construct[] Clustericon{ get;}
		void Add(Construct[] args,string meaning);
	}

	public enum ClusterPostion{
		INITIAL=1,
		MEDIAL=2,
		FINAL=3,
	}

	public enum SyllablePosition{
		INITIAL=1,
		MEDIAL=2,
		FINAL=3,
	}

	public enum MorphemicCategory{
		CLUSTER=1,
		SYLLABLE=2,
		MORPHEME=3
	}

	/// <summary>
	/// Syllabic systems are relatively flexible systems which allow combination of phonemes into clusters (with specific rules), combination of clusters into syllables
	/// (with other specific rules), and finally combination of syllables into morphemes (with a third set of specific rules).
	/// 
	/// You define the lower levels of the morphology via ONLY the rules, i.e. the system determines what phonemes fit the rules for you, you don't specify every syllable
	/// in the language yourself. At the higher level, you can of course assign meaning to compiled morphemes to your heart's content.
	/// 
	/// Clusters and syllables must be of bounded size, or the morphology will never finish construction. Morphemes may be of unbounded size (e.g. a morpheme can be
	/// as many syllables as you need, as long as there are a finite (although possibly very large) number of possible syllables in the language).
	/// </summary>
	public class SyllabicMorphology:Morphology{
		public Affiliate Affiliate{ get; }
		private List<Construct> morphemicon=new List<Construct>();
		private Dictionary<RuleSet,Construct[][]> morphemerules=new Dictionary<RuleSet, Construct[][]>();
		public Construct[] Morphemicon{ get { return this.morphemicon.ToArray(); } }
		public Construct[] Syllabicon{ get; }
		public Construct[] Clustericon{ get;}

		public SyllabicMorphology(Affiliate a,RuleSet[] clusterrules,RuleSet[] syllablerules,RuleSet[] morphemerules){
			this.Affiliate=a;
			this.Affiliate.Register(this);

			for(int i=0;i<clusterrules.Length;i++){
				clusterrules[i].AddMeta(new EnumeratedMeta(MorphemicCategory.CLUSTER));
			}
			for(int i=0;i<syllablerules.Length;i++){
				syllablerules[i].AddMeta(new EnumeratedMeta(MorphemicCategory.SYLLABLE));
			}
			for(int i=0;i<morphemerules.Length;i++){
				morphemerules[i].AddMeta(new EnumeratedMeta(MorphemicCategory.MORPHEME));
			}

			this.Clustericon=this.build(Layer.MORPHOLOGY_CLUSTER,r=>this.Affiliate.Phonology.Phonemicon.Where(x=>r.Applicable(x)).ToArray(),clusterrules);
			this.Syllabicon=this.build(Layer.MORPHOLOGY_SYLLABLE,r=>this.Clustericon.Where(x=>r.Applicable(x)).ToArray(),syllablerules);
			foreach(RuleSet ruleset in morphemerules) {
				List<Construct[]> options=new List<Construct[]>();
				this.morphemerules.Add(ruleset,ruleset.Rules.Select(x=>{return this.Syllabicon.Where(y => x.Applicable(y)).ToArray();}).ToArray());
			}
		}

		/// <summary>
		/// Variant of Add that allows you to specify phones as a string and propagates up the construct heirarchy for you.
		/// </summary>
		public void Add(string[] phones,string meaning){
			this.Affiliate.Phonetics.
		}

		/// <summary>
		/// This is a very freeform expectation. You may provide syllables, clusters, or just phonemes, but whatever you provide must still follow the language rules
		/// (e.g. if you provide phonemes, they better be arranged such that they agree with the syllable rules of the language).
		/// 
		/// The meaning is totally freeform text and is not used programmatically, it's just for your use.
		/// </summary>
		public void Add(Construct[] args,string meaning){
			List<Construct> syllables=new List<Construct>();
			List<Construct> clusters=new List<Construct>();
			List<Construct> phonemes=new List<Construct>();
			foreach(Construct arg in args) {
				if(this.Syllabicon.Contains(arg)) { // TODO will this be slow? Use a dictionary?
					process(ref clusters, ref syllables,this.Syllabicon);
					syllables.Add(arg);
				} else if(this.Clustericon.Contains(arg)) {
					process(ref phonemes, ref clusters,this.Clustericon);
					clusters.Add(arg);
				} else if(this.Affiliate.Phonology.Phonemicon.Contains(arg)) {
					phonemes.Add(arg);
				} else {
					throw new Exception("Unknown entity");
				}
			}
			Construct[] components=syllables.ToArray();
			foreach(KeyValuePair<RuleSet,Construct[][]> kvp in this.morphemerules) {
				bool matched=true;
				if(kvp.Value.Length==components.Length){
					for(int i=0;i<kvp.Value.Length;i++) {
						if(!kvp.Value[i].Contains(components[i])) {
							matched=false;
							break;
						}
					}
					if(matched) {
						this.morphemicon.Add(new Construct(
							Layer.MORPHOLOGY_MORPHEME,
							string.Join("",components.Select(x => x.Representation)),
							kvp.Key.Meta,
							components
						));
						components=null;
					}
				}
			}
			if(components!=null) {
				throw new Exception("Unknown Entity");
			}
		}

		/// <summary>
		/// Shuffle constructs up the morphemic heirarchy
		/// </summary>
		private void process(ref List<Construct> source, ref List<Construct> dest,Construct[] lookup) {
			if(source.Count>0) {
				Construct[] baked=source.ToArray();
				foreach(Construct entity in lookup) {
					if(entity.Equals(baked)) {
						dest.Add(entity);
						source.Clear();
						break;
					}
				}
				if(source.Count>0) {
					throw new Exception("Unknown Entity");
				}
			}
		}

		/// <summary>
		/// Enumerate possible morphemic constructs exhaustively based on the ruleset
		/// </summary>
		private Construct[] build(Layer layer,Selector selectdel,RuleSet[] rulesets) {
			List<Construct> clusters=new List<Construct>();
			foreach(RuleSet ruleset in rulesets) {
				Dictionary<Construct[],int> optionsets=new Dictionary<Construct[],int>();
				foreach(Rule rule in ruleset.Rules) {
					optionsets.Add(selectdel(rule),0);
				}
				while(true) {
					Construct[] components=optionsets.Select(kvp => kvp.Key[kvp.Value]).ToArray();
					clusters.Add(new Construct(layer,string.Join("",components.Select(x => x.Representation)),ruleset.Meta,components));
					int carry=1;
					Construct[][] iterable=optionsets.Keys.ToArray();
					foreach(Construct[] optionset in iterable) {
						optionsets[optionset]+=carry;
						carry=0;
						if(optionsets[optionset]>=optionset.Length) {
							optionsets[optionset]=0;
							carry=1;
						}
						else {
							break;
						}
					}
					if(carry==1) {
						break;
					}
				}
			}
			return clusters.ToArray();
		}

		public IEnumerable<Construct> Select(Condition c) {
			return this.Morphemicon.Where(x => c.Test(x));
		}
	}
}

