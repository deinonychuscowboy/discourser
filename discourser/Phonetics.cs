﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Discourser.Phonetics {

	public interface Phonetics:Facet{
		Construct[] Phoneticon{ get; }
	}
	public enum IPAPhoneticType{
		// these values are used so that vowels unambiguously begin with 100 and consonants with 101 (no leading zeros), and non-letters with 11 (suprasegmental marks with 110, composites with 111)
		VOWEL=4,
		CONSONANT=5,
		SUPRASEGMENTAL=6,
		COMPOSITE=7,
	}
	public enum IPAPhoneticSuprasegmentalType{
		STRESS=1,
		GEMINATION=2, // length variation
		DISTINCTION=3, // affrication, breaking, etc
		TONE=4,
		INTONATION=5,
	}
	public enum IPAPhoneticSuprasegmentalIntensity{
		MAXIMAL=0, // extra high, top, extra strong, extra long
		INCREASED=1, // high, strong, long
		NORMAL=2, // mid, normal
		DECREASED=3, // low, weak, short
		MINIMAL=4, // extra low, bottom, extra weak, extra short
	}
	public enum IPAPhoneticVowelRounding{
		UNROUNDED=0,
		ROUNDED=1,
	}
	public enum IPAPhoneticVowelHeight{
		CLOSE=1,
		NEAR_CLOSE=2,
		CLOSE_MID=3,
		MID=4,
		OPEN_MID=5,
		NEAR_OPEN=6,
		OPEN=7,
	}
	public enum IPAPhoneticVowelDepth{
		FRONT=1, // includes near-front since no language distinguishes between the two within the same height bracket
		CENTRAL=2,
		BACK=3, // includes near-back since no language distinguishes between the two within the same height bracket
	}
	[Flags]
	public enum IPAPhoneticVowelVariant{
		PLAIN=0,
		NASALIZED=1,
		RHOTACIZED=2,
	}
	public enum IPAPhoneticConsonantSource{
		EGRESSIVE=1,
		EJECTIVE=2,
		CLICK=3,
		INGRESSIVE=4,
	}
	public enum IPAPhoneticConsonantManner{
		STOP=1,
		FRICATIVE=2,
		APPROXIMANT=3,
		FLAP_TAP=4,
		TRILL=5,
		NASAL=6,
	}
	[Flags]
	public enum IPAPhoneticConsonantVariant{
		PLAIN=0,
		ASPIRATED=1, // TODO preaspiration?
		RHOTACIZED=2, // TODO
		// TODO does adding nasalized make sense here?
	}
	[Flags]
	public enum IPAPhoneticConsonantObject{
		LABIAL=1,
		LINGUAL=2,
		DENTAL=4, // always co-occurs with lingual
		LARYNGEAL=8,
	}
	public enum IPAPhoneticConsonantPlace{
		// sounds involving labial and dental objects (and sometimes lingual)
		PREALVEOLAR=1,
		// lingual-only sounds
		ALVEOLAR=2,
		POSTALVEOLAR=3, // now called palatoalveolar in modern IPA since postalveolar properly includes retroflex as well
		RETROFLEX=4,
		PALATAL=5, // includes alveolopalatal since there are only alveolopalatal sibilant and palatal non-sibilant fricatives and no other conflicting groups
		VELAR=6,
		// sounds involving laryngeal object
		UVULAR=7, // lingual and laryngeal
		PHARYNGEAL=8,
		EPIGLOTTAL=9,
		GLOTTAL=10,
	}
	public enum IPAPhoneticConsonantAirflow{
		NORMAL=0, // open for nonlingual consonants, apical for coronal consonants, dorsal for dorsal consonants
		LAMINAL=1, // non-apical and lateral TODO
		SIBILANT=2, // sibilant
		LATERAL=3, // lateral
		// TODO break into central/lateral and apical/laminal/grooved/dorsal?
	}
	public enum IPAPhoneticConsonantVoicing{
		VOICELESS=0,
		VOICED=1,
	}

	/// <summary>
	/// Representation of a linguistic sound via a superposition of all involved attributes.
	/// </summary>
	public abstract class SpatialPhoneticSuperposition{
		public IPAPhoneticType PType{ get; }
		public abstract int GetInt();
		public static implicit operator int(SpatialPhoneticSuperposition s){
			return s.GetInt();
		}
		public static implicit operator SpatialPhoneticSuperposition(int i){
			int j=i;
			while(j>=Math.Pow(2,SpatialPhoneticSuperposition.GetSpace(typeof(IPAPhoneticType)))) {
				j=j>>1;
			}
			if(j==(int)IPAPhoneticType.VOWEL) {
				return SpatialPhoneticSuperpositionVowel.GetSuperposition(i);
			} else if(j==(int)IPAPhoneticType.CONSONANT) {
				return SpatialPhoneticSuperpositionConsonant.GetSuperposition(i);
			} else if(j==(int)IPAPhoneticType.SUPRASEGMENTAL) {
				return SpatialPhoneticSuperpositionSuprasegmental.GetSuperposition(i);
			} else if(j==(int)IPAPhoneticType.COMPOSITE) {
				return SpatialPhoneticSuperpositionComposite.GetSuperposition(i);
			}
			throw new Exception("Unknown superposition type");
		}

		public SpatialPhoneticSuperposition(IPAPhoneticType type){
			this.PType=type;
		}

		protected static int GetSpace(Type enumType){
			int contain=((IEnumerable<int>)(typeof(Util.Enum<>).MakeGenericType(enumType).GetMethod(
				"GetIntegerValues",
				System.Reflection.BindingFlags.Public|System.Reflection.BindingFlags.Static
			).Invoke(
				null,
				null
			))).Max();
			int val=1;
			while((int)Math.Pow(2,val)<=contain) {
				val++;
			}
			return val;
		}

		protected static void Pack(ref int val,Enum enumerated){
			val=val<<SpatialPhoneticSuperposition.GetSpace(enumerated.GetType());
			val+=Convert.ToInt32(enumerated);
		}

		protected static Enum Unpack(ref int val,Type enumType){
			int size=SpatialPhoneticSuperposition.GetSpace(enumType);
			int mask=(int)Math.Pow(2,size)-1;
			int enumVal=val&mask;
			val=val>>size;
			return (Enum)Enum.ToObject(enumType,enumVal);
		}

		public virtual Enum[] GetEnums(){
			return new Enum[]{ this.PType };
		}
	}

	/// <summary>
	/// Vowel implementation of superposition including only vowel-related attributes
	/// </summary>
	public class SpatialPhoneticSuperpositionVowel:SpatialPhoneticSuperposition{
		public IPAPhoneticVowelHeight Height{get;}
		public IPAPhoneticVowelDepth Depth{get;}
		public IPAPhoneticVowelRounding Rounding{get;}
		public IPAPhoneticVowelVariant Variant{get;}

		public SpatialPhoneticSuperpositionVowel(IPAPhoneticVowelHeight height, IPAPhoneticVowelDepth depth, IPAPhoneticVowelRounding rounding, IPAPhoneticVowelVariant variant):base(IPAPhoneticType.VOWEL)
		{
			this.Height=height;
			this.Depth=depth;
			this.Rounding=rounding;
			this.Variant=variant;
		}

		public static SpatialPhoneticSuperposition GetSuperposition(int val){
			IPAPhoneticVowelVariant variant=(IPAPhoneticVowelVariant)SpatialPhoneticSuperpositionVowel.Unpack(ref val,typeof(IPAPhoneticVowelVariant));
			IPAPhoneticVowelRounding rounding=(IPAPhoneticVowelRounding)SpatialPhoneticSuperpositionVowel.Unpack(ref val,typeof(IPAPhoneticVowelRounding));
			IPAPhoneticVowelDepth depth=(IPAPhoneticVowelDepth)SpatialPhoneticSuperpositionVowel.Unpack(ref val,typeof(IPAPhoneticVowelDepth));
			IPAPhoneticVowelHeight height=(IPAPhoneticVowelHeight)SpatialPhoneticSuperpositionVowel.Unpack(ref val,typeof(IPAPhoneticVowelHeight));

			return new SpatialPhoneticSuperpositionVowel(height,depth,rounding,variant);
		}

		public override int GetInt() {
			int val=(int)this.PType;
			SpatialPhoneticSuperpositionVowel.Pack(ref val,this.Height);
			SpatialPhoneticSuperpositionVowel.Pack(ref val,this.Depth);
			SpatialPhoneticSuperpositionVowel.Pack(ref val,this.Rounding);
			SpatialPhoneticSuperpositionVowel.Pack(ref val,this.Variant);
			return val;
		}

		public override Enum[] GetEnums() {
			return base.GetEnums().Concat(new Enum[]{ this.Height, this.Depth, this.Rounding, this.Variant, }).ToArray();;
		}
	}

	/// <summary>
	/// Consonant implementation including only consonant-related attributes
	/// </summary>
	public class SpatialPhoneticSuperpositionConsonant:SpatialPhoneticSuperposition{
		public IPAPhoneticConsonantSource Source{get;}
		public IPAPhoneticConsonantManner Manner{get;}
		public IPAPhoneticConsonantVoicing Voicing{ get; }
		public IPAPhoneticConsonantVariant Variant{get;}
		public IPAPhoneticConsonantObject Object{ get; }
		public IPAPhoneticConsonantPlace Place{ get; }
		public IPAPhoneticConsonantAirflow Airflow{get;}

		public SpatialPhoneticSuperpositionConsonant(
			IPAPhoneticConsonantSource source,
			IPAPhoneticConsonantManner manner,
			IPAPhoneticConsonantVoicing voicing,
			IPAPhoneticConsonantVariant variant,
			IPAPhoneticConsonantObject obj,
			IPAPhoneticConsonantPlace place,
			IPAPhoneticConsonantAirflow lingualRegion
		):base(IPAPhoneticType.CONSONANT)
		{
			this.Source=source;
			this.Manner=manner;
			this.Voicing=voicing;
			this.Variant=variant;
			this.Object=obj;
			this.Place=place;
			this.Airflow=lingualRegion;
		}

		public static SpatialPhoneticSuperposition GetSuperposition(int val){
			IPAPhoneticConsonantAirflow lingualRegion=(IPAPhoneticConsonantAirflow)SpatialPhoneticSuperpositionConsonant.Unpack(ref val,typeof(IPAPhoneticConsonantAirflow));
			IPAPhoneticConsonantPlace place=(IPAPhoneticConsonantPlace)SpatialPhoneticSuperpositionConsonant.Unpack(ref val,typeof(IPAPhoneticConsonantPlace));
			IPAPhoneticConsonantObject obj=(IPAPhoneticConsonantObject)SpatialPhoneticSuperpositionConsonant.Unpack(ref val,typeof(IPAPhoneticConsonantObject));
			IPAPhoneticConsonantVariant variant=(IPAPhoneticConsonantVariant)SpatialPhoneticSuperpositionConsonant.Unpack(ref val,typeof(IPAPhoneticConsonantVariant));
			IPAPhoneticConsonantVoicing voicing=(IPAPhoneticConsonantVoicing)SpatialPhoneticSuperpositionConsonant.Unpack(ref val,typeof(IPAPhoneticConsonantVoicing));
			IPAPhoneticConsonantManner manner=(IPAPhoneticConsonantManner)SpatialPhoneticSuperpositionConsonant.Unpack(ref val,typeof(IPAPhoneticConsonantManner));
			IPAPhoneticConsonantSource source=(IPAPhoneticConsonantSource)SpatialPhoneticSuperpositionConsonant.Unpack(ref val,typeof(IPAPhoneticConsonantSource));

			return new SpatialPhoneticSuperpositionConsonant(source,manner,voicing,variant,obj,place,lingualRegion);
		}

		public override int GetInt() {
			int val=(int)this.PType;
			SpatialPhoneticSuperpositionConsonant.Pack(ref val,this.Source);
			SpatialPhoneticSuperpositionConsonant.Pack(ref val,this.Manner);
			SpatialPhoneticSuperpositionConsonant.Pack(ref val,this.Voicing);
			SpatialPhoneticSuperpositionConsonant.Pack(ref val,this.Variant);
			SpatialPhoneticSuperpositionConsonant.Pack(ref val,this.Object);
			SpatialPhoneticSuperpositionConsonant.Pack(ref val,this.Place);
			SpatialPhoneticSuperpositionConsonant.Pack(ref val,this.Airflow);
			return val;
		}

		public override Enum[] GetEnums() {
			return base.GetEnums().Concat(new Enum[]{ this.Source, this.Manner, this.Voicing, this.Variant, this.Object, this.Place, this.Airflow, }).ToArray();;
		}
	}

	/// <summary>
	/// Suprasegmental implementation for meta-phonetic attributes like tonality
	/// </summary>
	public class SpatialPhoneticSuperpositionSuprasegmental:SpatialPhoneticSuperposition{
		public IPAPhoneticSuprasegmentalType SType{get;}
		public IPAPhoneticSuprasegmentalIntensity Intensity{get;}

		public SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType type, IPAPhoneticSuprasegmentalIntensity intensity):base(IPAPhoneticType.SUPRASEGMENTAL)
		{
			this.SType=type;
			this.Intensity=intensity;
		}

		public static SpatialPhoneticSuperposition GetSuperposition(int val){
			IPAPhoneticSuprasegmentalIntensity intensity=(IPAPhoneticSuprasegmentalIntensity)SpatialPhoneticSuperpositionSuprasegmental.Unpack(ref val,typeof(IPAPhoneticSuprasegmentalIntensity));
			IPAPhoneticSuprasegmentalType type=(IPAPhoneticSuprasegmentalType)SpatialPhoneticSuperpositionSuprasegmental.Unpack(ref val,typeof(IPAPhoneticSuprasegmentalType));

			return new SpatialPhoneticSuperpositionSuprasegmental(type,intensity);
		}

		public override int GetInt() {
			int val=(int)this.PType;
			SpatialPhoneticSuperpositionSuprasegmental.Pack(ref val,this.SType);
			SpatialPhoneticSuperpositionSuprasegmental.Pack(ref val,this.Intensity);
			return val;
		}

		public override Enum[] GetEnums() {
			return base.GetEnums().Concat(new Enum[]{ this.SType, this.Intensity, }).ToArray();;
		}
	}

	/// <summary>
	/// Composite implementation for affricates and coarticulated sounds that are based on simpler phones
	/// </summary>
	public class SpatialPhoneticSuperpositionComposite:SpatialPhoneticSuperposition{
		public SpatialPhoneticSuperpositionCollection Components{get;}
		// if we end up with too many facets to pack into an int, this is a workable solution for other types of superpositions as well
		private static Dictionary<int,SpatialPhoneticSuperpositionCollection> known=new Dictionary<int, SpatialPhoneticSuperpositionCollection>();
		private static Dictionary<SpatialPhoneticSuperpositionCollection,int> invknown=new Dictionary<SpatialPhoneticSuperpositionCollection,int>();
		private static int pointer;

		public SpatialPhoneticSuperpositionComposite(params SpatialPhoneticSuperposition[] components):base(IPAPhoneticType.COMPOSITE)
		{
			this.Components=new SpatialPhoneticSuperpositionCollection(components);
			if(!SpatialPhoneticSuperpositionComposite.invknown.ContainsKey(this.Components)) {
				SpatialPhoneticSuperpositionComposite.invknown.Add(this.Components,SpatialPhoneticSuperpositionComposite.pointer);
				SpatialPhoneticSuperpositionComposite.known.Add(SpatialPhoneticSuperpositionComposite.pointer,this.Components);
				SpatialPhoneticSuperpositionComposite.pointer++;
			}
		}

		public SpatialPhoneticSuperpositionComposite(SpatialPhoneticSuperpositionCollection components):base(IPAPhoneticType.COMPOSITE)
		{
			this.Components=components;
			if(!SpatialPhoneticSuperpositionComposite.invknown.ContainsKey(this.Components)) {
				SpatialPhoneticSuperpositionComposite.invknown.Add(this.Components,SpatialPhoneticSuperpositionComposite.pointer);
				SpatialPhoneticSuperpositionComposite.known.Add(SpatialPhoneticSuperpositionComposite.pointer,this.Components);
				SpatialPhoneticSuperpositionComposite.pointer++;
			}
		}

		public static SpatialPhoneticSuperposition GetSuperposition(int val){
			int size=1;
			while((int)Math.Pow(2,size)<=val) {
				size++;
			}
			int mask=(int)Math.Pow(2,size-SpatialPhoneticSuperpositionComposite.GetSpace(typeof(IPAPhoneticType)))-1;

			SpatialPhoneticSuperpositionCollection components=SpatialPhoneticSuperpositionComposite.known[val&mask];

			return new SpatialPhoneticSuperpositionComposite(components);
		}

		public override int GetInt() {
			int val=(int)this.PType;
			int reference=SpatialPhoneticSuperpositionComposite.invknown[this.Components];

			int size=1;
			while((int)Math.Pow(2,size)<=reference) {
				size++;
			}

			return (val<<size)+reference;
		}

		public override Enum[] GetEnums() {
			return base.GetEnums().Concat(this.Components.GetEnums()).ToArray();;
		}
	}

	/// <summary>
	/// Collection of superpositions, implemented for its equality method (two collections are equal if they have the same superpositions in the same order)
	/// </summary>
	public class SpatialPhoneticSuperpositionCollection{
		public SpatialPhoneticSuperposition[] Components{get;}
		public SpatialPhoneticSuperpositionCollection(SpatialPhoneticSuperposition[] components){
			this.Components=components;
		}
		public override bool Equals(object obj) {
			if(obj is SpatialPhoneticSuperpositionCollection) {
				SpatialPhoneticSuperpositionCollection other=(SpatialPhoneticSuperpositionCollection)obj;
				if(this.Components.Length==other.Components.Length) {
					for(int i=0;i<this.Components.Length;i++) {
						if(this.Components[i]!=other.Components[i]) {
							return false;
						}
					}
					return true;
				}
				return false;
			}
			return base.Equals(obj);
		}

		public Enum[] GetEnums() {
			return this.Components.SelectMany(component => component.GetEnums()).ToArray();
		}
	}

	/// <summary>
	/// Representation of phonetics based on human-like phonology. This may not be complete or accurate for non-human vocal systems.
	/// 
	/// Spatial phonology is defined entirely by superpositions of phonetic attributes, which are mapped to integers. To help with determining the correct number for a given
	/// unique sound, you can use the SpatialPhoneticSuperposition classes, which are castable to ints, and the IPAPhonetic enums, which are obviously easier to read/figure out.
	/// </summary>
	public class SpatialPhonetics:Phonetics{
		public Affiliate Affiliate{get;}
		static Dictionary<int,string> Map{ get; }=new Dictionary<int,string>();
		static Dictionary<string,int> MapInverse{ get; }=new Dictionary<string,int>();
		public SpatialPhonetics(Affiliate a,params string[] ipaPhones):this(a,ipaPhones.Select(phone=>SpatialPhonetics.GetSpatialRepresentation(phone)).ToArray()){}
		public SpatialPhonetics(Affiliate a,params int[] spatialPhones){
			this.Affiliate=a;
			this.Affiliate.Register(this);
			foreach(int spatialPhone in spatialPhones) {
				string representation=SpatialPhonetics.GetIPARepresentation(spatialPhone);
				Construct construct=new Construct(Layer.PHONETICS,representation,((SpatialPhoneticSuperposition)spatialPhone).GetEnums().Select(enumEntry=>(Meta)new EnumeratedMeta(enumEntry)).ToArray(),null); // TODO conditions
				this.phoneticon.Add(construct);
			}
		}

		/// <summary>
		/// Get the textual IPA string for the phone.
		/// 
		/// IPA isn't really the best choice for computer-related tasks, since it's unicode-heavy (and thus hard to type) and inconsistent in the usage of some symbols, making it difficult to parse.
		/// However, it's the system I know best, and it has very broad language support thanks to being the unofficial standard system across the field of linguistics.
		/// Other systems like X-SAMPA and kirschenbaum I haven't evaluated to see if they can handle very obscure sounds that aren't known to be used in any language,
		/// whereas IPA is able to handle just about everything to the best of my knowledge.
		/// </summary>
		private static string GetIPARepresentation(int spatialPhone){
			if(SpatialPhonetics.Map.ContainsKey(spatialPhone)) {
				return SpatialPhonetics.Map[spatialPhone];
			} else {
				SpatialPhoneticSuperposition s=spatialPhone;
				if(s is SpatialPhoneticSuperpositionComposite) {
					SpatialPhoneticSuperpositionComposite c=(SpatialPhoneticSuperpositionComposite)s;
					string rep="";
					foreach(SpatialPhoneticSuperposition x in c.Components.Components) {
						rep+=SpatialPhonetics.GetIPARepresentation(x);
					}
					return rep;
				}
				throw new Exception("Unknown superposition"); // TODO handle additional diacritics
			}
		}

		/// <summary>
		/// Get the spatial superposition for an IPA string.
		/// 
		/// It would be nice if we could just parse the incoming IPA string, but IPA is very difficult to organize since it has things like the "raised" and "lowered" symbols,
		/// which change a sound's manner of articulation in a somewhat unpredictable way, and does other things like some voiced-unvoiced pairs having one symbol with the
		/// unvoiced member marked by a ring, and others (all clicks) having one symbol with the voiced member marked by a caron. It's not possible (or at least very hard)
		/// to treat IPA systematically because of this, we have to know all the non-conforming components unambiguously, hence the existence of this whole class instead of
		/// some sort of IPA parser.
		/// </summary>
		private static int GetSpatialRepresentation(string ipaRepresentation){
			if(SpatialPhonetics.MapInverse.ContainsKey(ipaRepresentation)) {
				return SpatialPhonetics.MapInverse[ipaRepresentation];
			} else {
				int start=0;
				int end=ipaRepresentation.Length;
				List<SpatialPhoneticSuperposition> components=new List<SpatialPhoneticSuperposition>();
				while(start!=ipaRepresentation.Length) {
					string slice=ipaRepresentation.Substring(start,end-start);
					if(SpatialPhonetics.MapInverse.ContainsKey(slice)) {
						components.Add(SpatialPhonetics.MapInverse[slice]);
						start=end;
						end=ipaRepresentation.Length;
					} else {
						end--;
					}
					if(end<=start&&end!=ipaRepresentation.Length) {
						throw new Exception("Unable to match symbol: "+slice); // TODO handle additional diacritics
					}
				}
				return new SpatialPhoneticSuperpositionComposite(new SpatialPhoneticSuperpositionCollection(components.ToArray()));
			}
		}

		public IEnumerable<Construct> Select(Condition c) {
			return this.Phoneticon.Where(x => c.Test(x));
		}
		private List<Construct> phoneticon=new List<Construct>();
		public Construct[] Phoneticon {get{ return this.phoneticon.ToArray(); }}

		static SpatialPhonetics(){
			// vowels are easy since they're pretty much exhaustive (added representations below for the front near-open rounded vowel and back near-open vowels so that they are)
			string[] vowels=new string[] {
				"i", "y", "ɨ", "ʉ", "ɯ", "u", "ɪ", "ʏ", "ɪ̈", "ʊ̈", "ɯ̽", "ʊ", "e", "ø", "ɘ", "ɵ", "ɤ", "o", "e̞", "ø̞", "ə", "ɵ̞", "ɤ̞", "o̞", "ɛ", "œ", "ɜ", "ɞ", "ʌ", "ɔ", "æ", "œ̞", "ɐ", "ɞ̞", "ʌ̞", "ɔ̞", "a", "ɶ", "ä", "ɒ̈", "ɑ", "ɒ"
			};
			int i=0;
			foreach(IPAPhoneticVowelHeight height in Enum.GetValues(typeof(IPAPhoneticVowelHeight))) {
				foreach(IPAPhoneticVowelDepth depth in Enum.GetValues(typeof(IPAPhoneticVowelDepth))) {
					foreach(IPAPhoneticVowelRounding rounding in Enum.GetValues(typeof(IPAPhoneticVowelRounding))) {
						SpatialPhonetics.Map.Add(
							new SpatialPhoneticSuperpositionVowel(height,depth,rounding,IPAPhoneticVowelVariant.PLAIN),
							vowels[i]
						);
						SpatialPhonetics.Map.Add(
							new SpatialPhoneticSuperpositionVowel(height,depth,rounding,IPAPhoneticVowelVariant.NASALIZED),
							vowels[i]+"̴"
						);
						SpatialPhonetics.Map.Add(
							new SpatialPhoneticSuperpositionVowel(height,depth,rounding,IPAPhoneticVowelVariant.RHOTACIZED),
							vowels[i]+"˞"
						);
						SpatialPhonetics.Map.Add(
							new SpatialPhoneticSuperpositionVowel(
								height,
								depth,
								rounding,
								IPAPhoneticVowelVariant.NASALIZED|IPAPhoneticVowelVariant.RHOTACIZED
							),
							vowels[i]+"̴"+"˞"
						);
						i++;
					}
				}
			}

			SpatialPhonetics.egressives();
			SpatialPhonetics.ejectives();
			SpatialPhonetics.clicks();
			SpatialPhonetics.ingressives();
			SpatialPhonetics.suprasegmentals();
			SpatialPhonetics.coarticulants();
			foreach(KeyValuePair<int,string> kvp in SpatialPhonetics.Map) {
				if(kvp.Value!=null) {
					SpatialPhonetics.MapInverse.Add(kvp.Value,kvp.Key);
				}
			}
		}

		private static void voicingAspiration(IPAPhoneticConsonantSource source,IPAPhoneticConsonantPlace place,IPAPhoneticConsonantAirflow region,IPAPhoneticConsonantObject obj,IPAPhoneticConsonantManner manner,string symbol=null,string symbol2=null) {
			foreach(Tuple<string,IPAPhoneticConsonantVoicing> a in new Tuple<string,IPAPhoneticConsonantVoicing>[] {
				new Tuple<string,IPAPhoneticConsonantVoicing>(symbol2??symbol,IPAPhoneticConsonantVoicing.VOICED),
				new Tuple<string,IPAPhoneticConsonantVoicing>(symbol!=null?symbol2!=null?symbol:symbol+"̥":null,IPAPhoneticConsonantVoicing.VOICELESS)
			}) {
				foreach(Tuple<string,IPAPhoneticConsonantVariant> b in new Tuple<string,IPAPhoneticConsonantVariant>[] {
					new Tuple<string,IPAPhoneticConsonantVariant>(a.Item1,IPAPhoneticConsonantVariant.PLAIN),
					new Tuple<string,IPAPhoneticConsonantVariant>(a.Item1!=null?a.Item1+"ʰ":null,IPAPhoneticConsonantVariant.ASPIRATED)
				}) {
					SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,a.Item2,b.Item2,obj,place,region),b.Item1);
				}
			}
		}

		private static void voicingAspirationGlottalization(IPAPhoneticConsonantSource source,IPAPhoneticConsonantPlace place,IPAPhoneticConsonantAirflow region,IPAPhoneticConsonantObject obj,IPAPhoneticConsonantManner manner,string symbol=null,string symbol2=null) {
			foreach(Tuple<string,IPAPhoneticConsonantVoicing> a in new Tuple<string,IPAPhoneticConsonantVoicing>[] {
				new Tuple<string,IPAPhoneticConsonantVoicing>(symbol2??symbol,IPAPhoneticConsonantVoicing.VOICED),
				new Tuple<string,IPAPhoneticConsonantVoicing>(symbol!=null?symbol2!=null?symbol:symbol+"̥":null,IPAPhoneticConsonantVoicing.VOICELESS)
			}) {
				foreach(Tuple<string,IPAPhoneticConsonantVariant,IPAPhoneticConsonantObject> b in new Tuple<string,IPAPhoneticConsonantVariant,IPAPhoneticConsonantObject>[] {
					new Tuple<string,IPAPhoneticConsonantVariant,IPAPhoneticConsonantObject>(a.Item1,IPAPhoneticConsonantVariant.PLAIN,obj),
					new Tuple<string,IPAPhoneticConsonantVariant,IPAPhoneticConsonantObject>(a.Item1!=null?a.Item1+"ʰ":null,IPAPhoneticConsonantVariant.ASPIRATED,obj),
					new Tuple<string,IPAPhoneticConsonantVariant,IPAPhoneticConsonantObject>(a.Item1!=null?a.Item1+"ˀ":null,IPAPhoneticConsonantVariant.PLAIN,obj|IPAPhoneticConsonantObject.LARYNGEAL),
					new Tuple<string,IPAPhoneticConsonantVariant,IPAPhoneticConsonantObject>(a.Item1!=null?a.Item1+"ˀ"+"ʰ":null,IPAPhoneticConsonantVariant.ASPIRATED,obj|IPAPhoneticConsonantObject.LARYNGEAL)
				}) {
					SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,a.Item2,b.Item2,b.Item3,place,region),b.Item1);
				}
			}
		}

		private static void coronalSpread(IPAPhoneticConsonantSource source,IPAPhoneticConsonantAirflow region,IPAPhoneticConsonantManner manner,string symbol=null,string symbol2=null) {

			IPAPhoneticConsonantObject obj=IPAPhoneticConsonantObject.LINGUAL;
			IPAPhoneticConsonantPlace place=IPAPhoneticConsonantPlace.ALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,symbol,symbol2);

			symbol=symbol!=null?symbol+"̟":null;
			symbol2=symbol2!=null?symbol2+"̟":null;
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.DENTAL;
			place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,symbol,symbol2);

			symbol=symbol!=null?symbol+"̠":null;
			symbol2=symbol2!=null?symbol2+"̠":null;
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,symbol,symbol2);
		}

		private static void coarticulants(){
			// special coarticulation references are only needed for coarticulated consonants that use a single symbol to represent the whole sound. Normal affricates and rarer coarticulated consonants that use symbols for their component sounds joined by a tie bar (in very correct IPA, or omitted in less formal contexts) do not need to be explicitly defined as long as their component sounds have been.

			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.FRICATIVE,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.FRICATIVE,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.ALVEOLAR,
						IPAPhoneticConsonantAirflow.SIBILANT
					)
				),
				"ɧ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"ʍ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"w"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.ALVEOLAR,
						IPAPhoneticConsonantAirflow.LATERAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.LATERAL
					)
				),
				"ɫ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.PALATAL,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"ɥ̊"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.PLAIN,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.PALATAL,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"ɥ"
			);

			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.FRICATIVE,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.FRICATIVE,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.ALVEOLAR,
						IPAPhoneticConsonantAirflow.SIBILANT
					)
				),
				"ɧʰ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"ʍʰ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"wʰ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.ALVEOLAR,
						IPAPhoneticConsonantAirflow.LATERAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.VELAR,
						IPAPhoneticConsonantAirflow.LATERAL
					)
				),
				"ɫʰ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICELESS,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.PALATAL,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"ɥ̊ʰ"
			);
			SpatialPhonetics.Map.Add(
				new SpatialPhoneticSuperpositionComposite(
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LABIAL,
						IPAPhoneticConsonantPlace.PREALVEOLAR,
						IPAPhoneticConsonantAirflow.NORMAL
					),
					new SpatialPhoneticSuperpositionSuprasegmental(
						IPAPhoneticSuprasegmentalType.DISTINCTION,
						IPAPhoneticSuprasegmentalIntensity.MINIMAL
					),
					new SpatialPhoneticSuperpositionConsonant(
						IPAPhoneticConsonantSource.EGRESSIVE,
						IPAPhoneticConsonantManner.APPROXIMANT,
						IPAPhoneticConsonantVoicing.VOICED,
						IPAPhoneticConsonantVariant.ASPIRATED,
						IPAPhoneticConsonantObject.LINGUAL,
						IPAPhoneticConsonantPlace.PALATAL,
						IPAPhoneticConsonantAirflow.NORMAL
					)
				),
				"ɥʰ"
			);
		}

		private static void suprasegmentals(){
			// tonal and intonation characters
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.TONE,IPAPhoneticSuprasegmentalIntensity.MAXIMAL),"˥");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.TONE,IPAPhoneticSuprasegmentalIntensity.INCREASED),"˦");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.TONE,IPAPhoneticSuprasegmentalIntensity.NORMAL),"˧");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.TONE,IPAPhoneticSuprasegmentalIntensity.DECREASED),"˨");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.TONE,IPAPhoneticSuprasegmentalIntensity.MINIMAL),"˩");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.INTONATION,IPAPhoneticSuprasegmentalIntensity.MAXIMAL),"↗");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.INTONATION,IPAPhoneticSuprasegmentalIntensity.INCREASED),"↑");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.INTONATION,IPAPhoneticSuprasegmentalIntensity.NORMAL),null); // constant tone not indicated
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.INTONATION,IPAPhoneticSuprasegmentalIntensity.DECREASED),"↓");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.INTONATION,IPAPhoneticSuprasegmentalIntensity.MINIMAL),"↘");

			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.STRESS,IPAPhoneticSuprasegmentalIntensity.MAXIMAL),"ˈ");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.STRESS,IPAPhoneticSuprasegmentalIntensity.INCREASED),"ˌ");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.STRESS,IPAPhoneticSuprasegmentalIntensity.NORMAL),null); // normal stress not indicated
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.STRESS,IPAPhoneticSuprasegmentalIntensity.DECREASED),null); // no concept of "more unstressed"
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.STRESS,IPAPhoneticSuprasegmentalIntensity.MINIMAL),null); // no concept of "more unstressed"
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.GEMINATION,IPAPhoneticSuprasegmentalIntensity.MAXIMAL),"ː");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.GEMINATION,IPAPhoneticSuprasegmentalIntensity.INCREASED),"ˑ");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.GEMINATION,IPAPhoneticSuprasegmentalIntensity.NORMAL),null); // normal length not indicated
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.GEMINATION,IPAPhoneticSuprasegmentalIntensity.DECREASED),"̐"); // invented symbol for "half-short"
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.GEMINATION,IPAPhoneticSuprasegmentalIntensity.MINIMAL),"̆");

			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.DISTINCTION,IPAPhoneticSuprasegmentalIntensity.MAXIMAL),"‖"); // intonation break/shortest pause
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.DISTINCTION,IPAPhoneticSuprasegmentalIntensity.INCREASED),"|"); // foot/word break
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.DISTINCTION,IPAPhoneticSuprasegmentalIntensity.NORMAL),"."); // normal syllabic break
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.DISTINCTION,IPAPhoneticSuprasegmentalIntensity.DECREASED),"͜"); // linking, no break
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionSuprasegmental(IPAPhoneticSuprasegmentalType.DISTINCTION,IPAPhoneticSuprasegmentalIntensity.MINIMAL),"͡"); // affrication and coarticulation (affrication occurs when a plosive and a fricative are pronounced as one unit, coarticulation when a plosive and a plosive or a fricative and a fricative are)
		}

		private static void ingressives(){
			// the full space of ingressive speech could properly represent the same or nearly the same space as egressive (normal) speech, but the implosive consonants (stops)
			// are the only commonly used ones, with possible rare ingressive fricatives, so other categories will not be represented.
			IPAPhoneticConsonantSource source=IPAPhoneticConsonantSource.INGRESSIVE;
			IPAPhoneticConsonantPlace place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			IPAPhoneticConsonantAirflow region=IPAPhoneticConsonantAirflow.NORMAL;
			IPAPhoneticConsonantManner manner=IPAPhoneticConsonantManner.STOP;
			// IPA doesn't have a dedicated implosive diacritic, so I'm using the nonstandard ᷾ diacritic that's sometimes used to represent ingressive airstream for extension symbols.

			IPAPhoneticConsonantObject obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɓ");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɓ̪");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɗ̼");

			SpatialPhonetics.coronalSpread(source,region,manner,"ɗ");

			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ᶑ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʄ");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɠ");
			place=IPAPhoneticConsonantPlace.UVULAR;
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.LARYNGEAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʛ");
			place=IPAPhoneticConsonantPlace.PHARYNGEAL;
			obj=IPAPhoneticConsonantObject.LARYNGEAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner); // assumed to be impossible since pharyngeal stops are impossible
			place=IPAPhoneticConsonantPlace.EPIGLOTTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʡ᷾");
			place=IPAPhoneticConsonantPlace.GLOTTAL;
			// glottal stop can only be voiceless
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʔ᷾");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.ASPIRATED,obj,place,region),"ʔ᷾ʰ");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICED,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICED,IPAPhoneticConsonantVariant.ASPIRATED,obj,place,region),null);

			// implosive fricatives aren't used in any known language, but are occasionally discussed, so here we go with some symbols for them just in case.

			manner=IPAPhoneticConsonantManner.FRICATIVE;

			source=IPAPhoneticConsonantSource.INGRESSIVE;
			place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			region=IPAPhoneticConsonantAirflow.NORMAL;

			// sonorant fricatives
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɸ᷾","β᷾");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"f᷾","v᷾");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ̼᷾","ð̼᷾");

			region=IPAPhoneticConsonantAirflow.SIBILANT; // sibilant fricatives (impossible for labial sounds)
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			region=IPAPhoneticConsonantAirflow.LATERAL; // lateral fricatives (impossible for nonlingual sounds)
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɬ̼᷾","ɮ̼᷾");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			region=IPAPhoneticConsonantAirflow.SIBILANT;
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.ALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"s᷾","z᷾");
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.DENTAL;
			place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"̟s᷾","z̟᷾");
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʃ᷾","ʒ᷾");
			region=IPAPhoneticConsonantAirflow.NORMAL;
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.ALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ̠᷾","ð̠᷾");
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.DENTAL;
			place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ᷾","ð᷾");
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɹ̠᷾˔");

			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.coronalSpread(source,region,manner,"ɬ᷾","ɮ᷾");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɻ᷾˔");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ç᷾","ʝ᷾");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"x᷾","ɣ᷾");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʂ᷾","ʐ᷾");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɕ᷾","ʑ᷾");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɭ᷾˔");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʎ̝᷾");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ̝᷾");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.UVULAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"χ᷾","ʁ᷾");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ̠̝᷾");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.PHARYNGEAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ħ᷾","ʕ᷾");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.EPIGLOTTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʢ᷾");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.GLOTTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"h᷾","ɦ᷾");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
		}

		private static void clicks(){
			// clicks can be aspirated and glottalized ("ejective")
			// glottalization will be represented by adding a laryngeal object to the click (there are no other laryngeal or lingual-laryngeal clicks)
			IPAPhoneticConsonantSource source=IPAPhoneticConsonantSource.CLICK;
			IPAPhoneticConsonantPlace place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			IPAPhoneticConsonantAirflow region=IPAPhoneticConsonantAirflow.NORMAL;

			IPAPhoneticConsonantObject obj=IPAPhoneticConsonantObject.LABIAL;

			IPAPhoneticConsonantManner manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ʘ̃");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ʘ","ʘ̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ʘ̪̃");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ʘ̪","ʘ̪̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ʘ̼̃");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ʘ̼","ʘ̼̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̼̃");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̼","ǁ̼̬");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.DENTAL|IPAPhoneticConsonantObject.LINGUAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǀ̃");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǀ","ǀ̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̟̃");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̟","ǁ̬̟");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.ALVEOLAR;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǃ̃");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"!","!̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̃");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ","ǁ̬");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǃ̠̃");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"!̠","!̠̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̠̃");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̠","ǁ̬̠");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.RETROFLEX;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǃ̃˞");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǃ˞","!̬˞");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̃˞");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ˞","ǁ̬˞");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.PALATAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǂ̃");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǂ","ǂ̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁ̃ʲ");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner,"ǁʲ","ǁ̬ʲ");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			// nothing further back can be pronounced in a distinguishable way

			place=IPAPhoneticConsonantPlace.VELAR;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspirationGlottalization(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.UVULAR;
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.LARYNGEAL;
			// below here the larynx is already involved, so these clicks could not be glotallized even if they were possible

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.PHARYNGEAL;
			obj=IPAPhoneticConsonantObject.LARYNGEAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.EPIGLOTTAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.GLOTTAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
		}

		private static void ejectives(){
			IPAPhoneticConsonantSource source=IPAPhoneticConsonantSource.EJECTIVE;
			IPAPhoneticConsonantPlace place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			IPAPhoneticConsonantAirflow region=IPAPhoneticConsonantAirflow.NORMAL;

			// aspiration is usually contrastive with ejection where both are contrastive with another category like tenuis, so aspiration is not represented here
			// ejectives are also almost always voiceless, so no voiced versions are provided
			IPAPhoneticConsonantObject obj=IPAPhoneticConsonantObject.LABIAL;
			IPAPhoneticConsonantManner manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"p̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɸ̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"p̪̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"f̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"t̼̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"θ̼̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɬ̼̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.DENTAL|IPAPhoneticConsonantObject.LINGUAL;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"t̟̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"θ̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"s̟̕");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɬ̟̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.ALVEOLAR;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"t̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"θ̠̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"s̕");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɬ̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"t̠̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɹ̠̊˔̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʃ̕");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɬ̠̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʈ̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɻ̊˔̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʂ̕");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɭ̊˔̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.PALATAL;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"c̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ç̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ɕ̕");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʎ̥˔̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.VELAR;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"k̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"x̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʟ̝̊̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.LARYNGEAL;
			place=IPAPhoneticConsonantPlace.UVULAR;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"q̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʟ̠̝̊̕");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			obj=IPAPhoneticConsonantObject.LARYNGEAL;
			place=IPAPhoneticConsonantPlace.PHARYNGEAL;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ħ̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.EPIGLOTTAL;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʡ̕");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʢ̊̕");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			place=IPAPhoneticConsonantPlace.GLOTTAL;
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			region=IPAPhoneticConsonantAirflow.NORMAL;
		}

		private static void egressives(){
			IPAPhoneticConsonantSource source=IPAPhoneticConsonantSource.EGRESSIVE;
			IPAPhoneticConsonantPlace place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			IPAPhoneticConsonantAirflow region=IPAPhoneticConsonantAirflow.NORMAL;

			IPAPhoneticConsonantManner manner=IPAPhoneticConsonantManner.NASAL;

			IPAPhoneticConsonantObject obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"m");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɱ");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"n̼");

			manner=IPAPhoneticConsonantManner.STOP;

			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"p","b");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"p̪","b̪");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"t̼","d̼");

			manner=IPAPhoneticConsonantManner.FRICATIVE;

			// sonorant fricatives
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɸ","β");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"f","v");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ̼","ð̼");

			region=IPAPhoneticConsonantAirflow.SIBILANT; // sibilant fricatives (impossible for labial sounds)
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;

			region=IPAPhoneticConsonantAirflow.LATERAL; // lateral fricatives (impossible for nonlingual sounds)
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɬ̼","ɮ̼");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			manner=IPAPhoneticConsonantManner.APPROXIMANT;

			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɸ̞","β̞");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʋ");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ̼̞","ð̼̞");

			region=IPAPhoneticConsonantAirflow.LATERAL; // lateral approximants (impossible for nonlingual sounds)
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"l̼");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			manner=IPAPhoneticConsonantManner.FLAP_TAP;

			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ⱱ̟");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ⱱ");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɾ̼");

			region=IPAPhoneticConsonantAirflow.LATERAL; // lateral flaps/taps (impossible for nonlingual sounds)
			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɺ̼");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			manner=IPAPhoneticConsonantManner.TRILL;

			obj=IPAPhoneticConsonantObject.LABIAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʙ");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.DENTAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ⱱ̞");
			obj=IPAPhoneticConsonantObject.LABIAL|IPAPhoneticConsonantObject.LINGUAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"r̼");

			// frontal coronal consonants (largely not distinguished, usually alveolar)
			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.coronalSpread(source,region,manner,"n");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.coronalSpread(source,region,manner,"t","d");

			manner=IPAPhoneticConsonantManner.FRICATIVE;
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.ALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"s","z");
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.DENTAL;
			place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"̟s","z̟");
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʃ","ʒ");
			region=IPAPhoneticConsonantAirflow.NORMAL;
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.ALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ̠","ð̠");
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.DENTAL;
			place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ","ð");
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɹ̠˔");

			manner=IPAPhoneticConsonantManner.APPROXIMANT;
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.ALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɹ");
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.DENTAL;
			place=IPAPhoneticConsonantPlace.PREALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"θ̞","ð̞");
			obj=IPAPhoneticConsonantObject.LINGUAL;
			place=IPAPhoneticConsonantPlace.POSTALVEOLAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɹ̠");

			manner=IPAPhoneticConsonantManner.FLAP_TAP;
			SpatialPhonetics.coronalSpread(source,region,manner,"ɾ");
			manner=IPAPhoneticConsonantManner.TRILL;
			SpatialPhonetics.coronalSpread(source,region,manner,"r");

			region=IPAPhoneticConsonantAirflow.LATERAL;
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.coronalSpread(source,region,manner,"ɬ","ɮ");
			manner=IPAPhoneticConsonantManner.APPROXIMANT;
			SpatialPhonetics.coronalSpread(source,region,manner,"l");
			manner=IPAPhoneticConsonantManner.FLAP_TAP;
			SpatialPhonetics.coronalSpread(source,region,manner,"ɺ");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			// rear lingual consonants (coronal/dorsal)
			region=IPAPhoneticConsonantAirflow.NORMAL;
			obj=IPAPhoneticConsonantObject.LINGUAL;

			manner=IPAPhoneticConsonantManner.NASAL;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɳ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɲ");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ŋ");

			manner=IPAPhoneticConsonantManner.STOP;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʈ","ɖ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"c","ɟ");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"k","g");

			manner=IPAPhoneticConsonantManner.FRICATIVE;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɻ˔");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ç","ʝ");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"x","ɣ");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʂ","ʐ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɕ","ʑ");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɭ˔");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʎ̝");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ̝");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			manner=IPAPhoneticConsonantManner.APPROXIMANT;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɻ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"j");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɰ");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɭ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʎ");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			manner=IPAPhoneticConsonantManner.FLAP_TAP;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɽ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɺ̢");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʎ̮");
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ̆");
			region=IPAPhoneticConsonantAirflow.NORMAL;

			manner=IPAPhoneticConsonantManner.TRILL;

			place=IPAPhoneticConsonantPlace.RETROFLEX;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɽ̊ɽ̊","ɽɽ");
			place=IPAPhoneticConsonantPlace.PALATAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			place=IPAPhoneticConsonantPlace.VELAR;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);

			//uvular consonants
			region=IPAPhoneticConsonantAirflow.NORMAL;
			obj=IPAPhoneticConsonantObject.LINGUAL|IPAPhoneticConsonantObject.LARYNGEAL;
			place=IPAPhoneticConsonantPlace.UVULAR;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɴ");
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"q","ɢ");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"χ","ʁ");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ̠̝");
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.APPROXIMANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"χ̞","ʁ̞");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ̠");
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.FLAP_TAP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ɢ̆");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʟ̠̞");
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.TRILL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʀ");

			// laryngeal consonants
			region=IPAPhoneticConsonantAirflow.NORMAL;
			obj=IPAPhoneticConsonantObject.LARYNGEAL;
			place=IPAPhoneticConsonantPlace.PHARYNGEAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ħ","ʕ");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.APPROXIMANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ħ̞","ʕ̞");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.FLAP_TAP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.TRILL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);

			place=IPAPhoneticConsonantPlace.EPIGLOTTAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʡ","ʡ̬");
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʢ");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.APPROXIMANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʢ̞");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.FLAP_TAP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʡ̮","ʡ̮̬");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.TRILL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʜ","ʢʢ");

			place=IPAPhoneticConsonantPlace.GLOTTAL;

			manner=IPAPhoneticConsonantManner.NASAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			manner=IPAPhoneticConsonantManner.STOP;
			// glottal stop can only be voiceless
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),"ʔ");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICELESS,IPAPhoneticConsonantVariant.ASPIRATED,obj,place,region),"ʔʰ");
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICED,IPAPhoneticConsonantVariant.PLAIN,obj,place,region),null);
			SpatialPhonetics.Map.Add(new SpatialPhoneticSuperpositionConsonant(source,manner,IPAPhoneticConsonantVoicing.VOICED,IPAPhoneticConsonantVariant.ASPIRATED,obj,place,region),null);
			manner=IPAPhoneticConsonantManner.FRICATIVE;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"h","ɦ");
			region=IPAPhoneticConsonantAirflow.SIBILANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.APPROXIMANT;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner,"ʔ̞");
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.FLAP_TAP;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.LATERAL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
			region=IPAPhoneticConsonantAirflow.NORMAL;
			manner=IPAPhoneticConsonantManner.TRILL;
			SpatialPhonetics.voicingAspiration(source,place,region,obj,manner);
		}
	}
}

