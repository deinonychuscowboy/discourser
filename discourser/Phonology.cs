﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Discourser.Phonology {
	public interface Phonology:Facet{
		Construct[] Phonemicon{ get; }
	}
	public class IPABasedPhonology:Phonology{
		public Affiliate Affiliate{get;}
		private List<Construct> phonemicon=new List<Construct>();
		public Construct[] Phonemicon{ get { return this.phonemicon.ToArray(); } }

		public IPABasedPhonology(Affiliate a,IPAPhonologyInstruction[] language){
			this.Affiliate=a;
			this.Affiliate.Register(this);

			foreach(IPAPhonologyInstruction x in language) {
				Construct construct=x.Construct(this.Affiliate.Phonetics);
				if(construct.Layer!=Layer.PHONOLOGY) {
					throw new Exception();
				}
				this.phonemicon.Add(construct);
			}
		}

		public IEnumerable<Construct> Select(Condition c) {
			return this.Phonemicon.Where(x => c.Test(x));
		}
	}

	public class IPAPhonologyInstruction{
		private Construct construct;
		private Condition condition;
		public IPAPhonologyInstruction(Condition c){
			this.condition=c;
		}
		public IPAPhonologyInstruction(Construct c){
			this.construct=c;
		}
		public static implicit operator IPAPhonologyInstruction(Condition c){
			return new IPAPhonologyInstruction(c);
		}
		public static implicit operator IPAPhonologyInstruction(Construct c){
			return new IPAPhonologyInstruction(c);
		}
		public Construct Construct(Phonetics.Phonetics phonetics){
			Func<Construct> del=()=>{
				Construct[] phoneticElements=phonetics.Select(this.condition).ToArray();
				if(phoneticElements.Length==0){
					throw new Exception("No phones found meeting the criteria: "+string.Join(" ",this.condition.Selector.Select<Meta,string>(x=>Enum.GetName(((EnumeratedMeta)x).CategoryType,((EnumeratedMeta)x).Category))));
				}
				string s=phoneticElements.Aggregate<Construct,string>(null,(string carry,Construct c) => {
					if(carry==null){
						return c.Representation;
					}else{
						string computed=new string(carry.Where(x=>c.Representation.Contains(x)).ToArray());
						return computed!=""?computed:carry+"/"+c.Representation;
					}
				});
				if(s.Contains("/")){
					s="["+s+"]";
				}
				Meta[] commonPhoneticTraits=phoneticElements.Aggregate((IEnumerable<Meta>)null,(carry,x)=>{return carry==null?x.Meta:carry.Where(y=>x.Meta.Contains(y));}).ToArray();
				// TODO applicability
				return new Construct(Layer.PHONOLOGY,s,commonPhoneticTraits,phoneticElements);
			};
			return this.construct??del();
		}
	}
}

