﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Discourser.Util {
	public static class Enum<T>
	{
		public static IEnumerable<int> GetIntegerValues()
		{
			return Enum.GetValues(typeof(T)).Cast<T>().Select(x=>Convert.ToInt32(x));
		}   
	}
}

